<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::create(
            [
                'name' => 'Maryam Ali',
                'email' => 'maryam@bitcode.sa',
                'role' => 'ADMIN',
                'password' => bcrypt('Aba57727')
            ]
        );
    }
}
