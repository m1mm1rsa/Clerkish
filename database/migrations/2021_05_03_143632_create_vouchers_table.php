<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            $table->integer('number');
            $table->enum('type',['DEBIT' , 'PAYMENT']);//make routes
            $table->double('amount', 8, 2);
            $table->string('writtenAmount');
            $table->string('relatedPerson');//who gets the money
            $table->text('statment')->nullable();
            $table->enum('paymentType', ['CARD', 'CASH','TRANSFER','CHECK']);//restrict info in the controller.
            $table->json('paymentData')->nullable();//eavry type need a static form of info.
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
