<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('accountCode')->unique();
            $table->string('name');
            $table->enum('type', ['PAYABLE', 'RECEIVABLE', ' EXPENSE']);
            $table->boolean('suspended')->default(false);
            $table->boolean('main');
            $table->enum('close', ['BUDGET','INCOMEANDEXPENCE']);
            $table->foreignId('parentID')->nullable();
            $table->string('nextChildID')->nullable();
            $table->string('lastChildID')->nullable();
            $table->timestamps();
            
        });
        Schema::table('accounts', function (Blueprint $table) {
            $table->foreign('parentID')
            ->references('id')->on('accounts')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
