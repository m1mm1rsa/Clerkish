<?php

return [
    'index' => 'تم جلب بيانات يوميات الحسابات بنجاح',
    'show' => 'تم جلب بيانات يومية الحساب بنجاح',
    'inserted' => 'تم اضافة :name بنجاح',
    'notInserted' => 'حدث خطأ أثناء محاولة حفظ :name',
    'updated' => 'تم حفظ التغيرات على :name بنجاح',
    'notUpdated' => 'حدث خطأ أثناء محاولة حفظ التغيرات على :name',
    'noUpdates' => 'لا يوجد أي تحديثات على :name',
    'cannotUpdate' => 'لا تستطيع عمل أي تحديثات على القيد',
    'deleted' => 'تم حذف يومية الحساب بنجاح',
    'cannotDelete' => 'لا تستطيع حذف القيد',
    'notDeleted' => 'حدث خطأ أثناء محاولة حذف :name',
    'notFound' =>  'يومية الحساب غير موجودة',
];