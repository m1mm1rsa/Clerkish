<?php

return [
    "index" => "Journals have been fetched successfully",
    "show" => "Journal is fetched successfully",
    "inserted" => ":name is added successfully",
    "notInserted" => "Error: issue has happened while adding :name",
    "updated" => ":name information updated successfully",
    "notUpdated" => "Error: issue has happened while updating :name",
    "noUpdates" => "There are no updates on :name",
    "deleted" => "Journal deleted successfully",
    "notDeleted" => "Error occurred while deleting :name",
    "notFound" => "Journal is not found",
    "cannotUpdate" => "Can not update the journal",
    "cannotDelete" => "Can not delete the journal",
];