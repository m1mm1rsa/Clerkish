<?php

return [
    'index' => 'Accounts have been fetched successfully',
    'show' => 'Account is fetched successfully',
    'inserted' => ':name is added successfully',
    'notInserted' => 'Error: issue has happened while adding :name',
    'updated' => ':name information updated successfully',
    'notUpdated' => 'Error: issue has happened while updating :name',
    'noUpdates' => 'There are no updates on :name',
    'deleted' => 'Account deleted successfully',
    'notDeleted' => 'Error occurred while deleting :name',
    'notFound' => 'Account is not found',
    'wrongInsersion' =>  'wrong data are entered',
    'notAllowed' => 'You are not allowed to do this process',
    'wrongAccountCode' => 'There is a mistake in the account code',
    'doublicatedAccountCode' => 'Doublicated account code',
];