<?php
namespace App\Http\Resources\UserResources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResources extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            
        ];
    }
}