<?php

namespace App\Http\Resources\JournalResources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\DailyJournal;

class ViewJournalResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'number' => $this->number,
            'statment' => $this->statment,
            'type' => $this->type,
            'date' => $this->date,
            
        ];
    }
}