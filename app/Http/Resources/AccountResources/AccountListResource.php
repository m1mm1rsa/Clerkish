<?php

namespace App\Http\Resources\AccountResources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountListResource extends JsonResource
{
 
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'accountCode'=> $this->accountCode,
            'type'=> $this->type,
            'nextChildID'=> $this->nextChildID,
            'close'=> $this->close,
        ];
    }
}
