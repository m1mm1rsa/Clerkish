<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Admin
{
    public function handle(Request $request, Closure $next)
    {
        $userRole = auth()->user()->role;

        if ($userRole != 'ADMIN') {
            return Controller::getResponse(
                __('userMessages.notAllowed'),
                401
            );
        }

        return $next($request);
    }
}
