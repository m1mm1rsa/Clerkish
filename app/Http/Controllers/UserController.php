<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResources\UserIndexResource;
use App\Http\Resources\UserResources\UserProfileResources;
use App\Models\User;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index() // return all records
    {
        $users = User::all();
        $users = UserIndexResource::collection($users);
        return parent::getPaginatedResopnse(
            __('userMessages.index'),
            $users
        );
        
    }

    public function insert(Request $request) //add new record
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => [
                'required',
                Rule::in(['ADMIN', 'admin', 'ACCOUNTENT', 'accountent', 'MEMBER', 'member']),
            ],
            'password' => 'required'
        ]);

        $user =  new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = strtoupper($request->role);
        $user->password = bcrypt($request->password);
        

        if (!$user->save()) {
            return parent::getResponse(
                __('userMessages.cannotInsert'),
                304
            );
        }

        return response()->json($user, 201);
    }

    public function update(Request $request) //update specific record
    {
        $request->validate([
            'userID'  => 'required',
            'name' => 'required',
            'email' => 'required',
            'role' => [
                'required',
                Rule::in(['ADMIN', 'admin', 'ACCOUNTENT', 'accountent', 'MEMBER', 'member']),
            ],
            'password' => 'required'
        ]);

        $user = User::find($request->userID);

        if (!$user) {
            return parent::getResopnse(
                __('userMessages.notFound'),
                404
            );
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);

        if (!$user->isDirty()) {
            return parent::getResponse(
                __('userMessages.noUpdates'),
                200
            );
        }


        if (!$user->save()) {
            return parent::getResponse(
                __('accountMessages.cannotUpdate'),
                304
            );
        }

        return response()->json($user, 200);
    }


    public function show($userID)
    {
        $user = User::find($userID);

        if (!$user) {
            return parent::getResponse(
                __('userMessages.notFound'),
                404
            );
        }
        $user = UserProfileResources::collection([$user]);

        return parent::getResponse(
            __('userMessages.show'),
            200,
            $user
        );
    }

    public function myProfile($email)
    {
        $user = User::whereEmail($email)->first();

        if (!$user) {
            return parent::getResponse(
                __('userMessages.notFound'),
                404
            );
        }
        $user = UserProfileResources::collection([$user]);

        return parent::getResponse(
            __('userMessages.show'),
            200,
            $user
        );
    }

    public function delete($userID) //delete specific record
    {
        $user = User::find($userID);

        if (!$user) {
            return parent::getResponse(
                __('userMessages.notFound'),
                404
            );
        }

        if (!$user->delete()) {
            return parent::getResponse(
                __('userMessages.deleted'),
                304,
                $userID
            );
        }

        return parent::getResponse(
            __('userMessages.deleted'),
            200
        );
    }

    public function login(Request $request)
    { 
        $request->validate([
        'email' => 'required',
        'password' => 'required'
        ]);
        
        $credentials = request(['email','password']);

        if(!Auth::attempt($credentials)){
            return response(
                __('userMessages.failedLogin'),
            401);
        }

        $user = User::whereEmail($request->email)->first();
        
        $token = $user->createToken('login using email & password');

        return parent::getResponse(
        __('userMessages.successLogin'),
        200,
        [
            'token' => $token->plainTextToken,
            'role' => $user->role
        ]
        );

    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return parent::getResponse(
            __('userMessages.logout')
        );
    }
    
     public function resetPassword(Request $request)
    {
        $request->validate([
            'userID' => 'required',
            'password' => 'required|min:8|max:14'
        ]);
        $user = User::find($request->userID);

        
        if (!$user) {
            return parent::getResopnse(
                __('userMessages.notFound'),
                404
            );
        }

        $user->password = bcrypt($request->password);

        if (!$user->isDirty()) {
            return parent::getResponse(
                __('userMessages.noUpdates'),
                200
            );
        }


        if (!$user->save()) {
            return parent::getResponse(
                __('accountMessages.cannotUpdate'),
                304
            );
        }

        return response()->json($user, 200);
    }

    
     public function changerole(Request $request)
    {
         $request->validate([
            'userID' => 'required',
            'role' => 'required'
        ]);
         
         $user = User::find($request->userID);
        
         if (!$user) {
            return parent::getResopnse(
                __('userMessages.notFound'),
                404
            );
        }

        $user->role = $request->role;

        if (!$user->isDirty()) {
            return parent::getResponse(
                __('userMessages.noUpdates'),
                200
            );
        }


        if (!$user->save()) {
            return parent::getResponse(
                __('accountMessages.cannotUpdate'),
                304
            );
        }

        return response()->json($user, 200);
    }
    
}
