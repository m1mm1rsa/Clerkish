<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AccountsJournal;
use App\Models\Account;
use App\Models\DailyJournal;

class AccountsJournalController extends Controller
{
    public function index()
    {
        $accountJournals = AccountsJournal::all();

        return parent::getPaginatedResopnse(
            __('accountsJournal.index'),
            $accountJournals
        );
    }


    public function insert(Request $request)
    {
        $request->validate([
            'accountID' => 'required',
            'journalID' => 'required',
            'amount' => 'required|numeric',
            'credit' => 'required|boolean',
        ]);

        $account = Account::find($request->accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                $account
            );
        }

        $journal = DailyJournal::find($request->journalID);

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                $journal
            );
        }

        $accountJournal = $account->journals()->wherePivot('journalID', $request->journalID)->get();
        $relatedAccounts = count($accountJournal);

        $account->journals()->attach($request->journalID, [
            'credit' => $request->credit,
            'amount' => $request->amount,
            'statment' => $request->statment
        ]);

        $accountJournal = $account->journals()->wherePivot('journalID', $request->journalID)->get();

        if (!count($accountJournal) || count($accountJournal) <= $relatedAccounts) {
            return response()->json('accountMessages.notFound', 304);
        }
        return response()->json($accountJournal, 201);
    }

    public function update(Request $request)
    {
        $request->validate([
            'accountID' => 'required',
            'journalID' => 'required',
            'amount' => 'required|numeric',
            'credit' => 'required|boolean',
        ]);

        $account = Account::find($request->accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                404
            );
        }

        $journal = DailyJournal::find($request->journalID);

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                404
            );
        }

        $accountJournal = $account->journals()->wherePivot('journalID', $request->journalID)->get();
        $relatedAccounts = count($accountJournal);

        $account->journals()->updateExistingPivot($request->journalID, [
            'credit' => $request->credit,
            'amount' => $request->amount,
            'statment' => $request->statment
        ]);

        return response()->json($accountJournal, 201);
    }

    public function show($accountID, $journalID)
    {
        $account = Account::find($accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                404
            );
        }

        $journal = DailyJournal::find($journalID);

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                404
            );
        }

        $accountJournal = $account->journals()->wherePivot('journalID', $journalID)->get();
        
        if (!count($accountJournal)) {
            return response()->json('accountMessages.notFound', 304);
        }
        return response()->json($accountJournal, 201);
    }

    public function delete($accountID, $journalID)
    {
        $account = Account::find($accountID);

        if (!$account) {
            return parent::getResponse(
                __('$accountMessages.NotFound'),
                404
            );
        }

        $journal = $account->journals()->where('id', $journalID)->first();

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                404
            );
        }

        $account->journals()->detach(
            $journal->id
        );

        return parent::getResponse(
            __('accountJournalMessages.deleted'),
            200
        );
    }

    public function addStatment(Request $request)
    {
        $request->validate([
            'accountID' => 'required',
            'journalID' => 'required',
            'statment' => 'required'
        ]);

        $account = Account::find($request->accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                404
            );
        }

        $journal = DailyJournal::find($request->journalID);

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                404
            );
        }

        $accountJournal = $account->journals()->wherePivot('journalID', $request->journalID)->get();
        $relatedAccounts = count($accountJournal);

        $account->journals()->attach($request->journalID, [
            'statment' => $request->statment
        ]);

        $isDirty = $accountJournal->isDirty();

        if (!$isDirty) {
            return parent::getResponse(
                __('accountJournalMessages.noUpdates'),
                200
            );
        }

        if (!count($accountJournal) || count($accountJournal) <= $relatedAccounts) {
            return parent::getResponse(
                __('accountJournalMessages.notUpdated'),
                304,
                $accountJournal
            );
        }

        return response()->json($accountJournal, 201);
    }
}
