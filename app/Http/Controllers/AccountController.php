<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Resources\AccountResources\AccountListResource;

class AccountController extends Controller
{
    public function index()
    {
        $accounts = Account::all();

        return parent::getPaginatedResopnse(
            __('accountMessages.index'),
            $accounts
        );
    }

    public function lastAccountCode()
    { //retreve accounts code with no parents

        $accounts = Account::where('parentID', null)->get();
        $maxAccountCode = 0;

        foreach ($accounts as $account) {
            if ($maxAccountCode < $account->accountCode) {
                $maxAccountCode = $account->accountCode;
            }
        }

        $maxAccountCode += 1;

        return parent::getResponse(
            __('accountMessages.show'),
            200,
            $maxAccountCode
        );
    }

    public function insert(Request $request)
    {
        $request->validate([
            'accountCode' => 'required',
            'name' => 'required',
            'type' => [
                'required',
                Rule::in(['PAYABLE', 'payable', 'RECEIVABLE', 'receivable', ' EXPENSE', 'expense']),
            ],
            'close' => [
                'required',
                Rule::in(['BUDGET', 'budget', 'INCOMEANDEXPENCE', 'incomeandexpence']),
            ],
            'main' => 'required',
        ]);

        if ($request->parentID && $request->parentID != null) {
            $parent = Account::find($request->parentID);
            if (!$parent) {
                return parent::getResponse(
                    __('accountMessages.notFound'),
                    404
                );
            }

            if ($parent->close != $request->close || $parent->type != $request->type) {
                return parent::getResponse(
                    __('accountMessages.wrongInsersion'),
                    405
                );
            }

            if ($request->accountCode != $parent->nextChildID) {
                return parent::getResponse(
                    __('accountMessages.wrongAccountCode'),
                    405
                );
            }

            if ($request->accountCode > $parent->lastChildID) {
                return parent::getResponse(
                    __('accountMessages.wrongAccountCode'),
                    405
                );
            }

            $parent->nextChildID = (int)$parent->nextChildID + 1;
            $parent->save();
        }
        $isExist = Account::where('accountCode', $request->accountCode)->get();

        if (count($isExist)) {
            return parent::getResponse(
                __('accountMessages.doublicatedAccountCode'),
                405
            );
        }
        $nextChildID = null;
        $lastChildID = null;
        if ($request->main) {
            switch ($request->lastChildID) {
                case 9:
                    $nextChildID = $request->accountCode . '1';
                    $lastChildID = $request->accountCode . '9';
                    break;
                case 99:
                    $nextChildID = $request->accountCode . '01';
                    $lastChildID = $request->accountCode . '99';
                    break;
                case 999:
                    $nextChildID = $request->accountCode . '001';
                    $lastChildID = $request->accountCode . '999';
                    break;
                case 9999:
                    $nextChildID = $request->accountCode . '0001';
                    $lastChildID = $request->accountCode . '9999';
                    break;
            }
        }
        $account = new Account;
        $account->accountCode = $request->accountCode;
        $account->name = $request->name;
        $account->type = strtoupper($request->type);
        $account->main = $request->main;
        $account->close = strtoupper($request->close);
        $account->nextChildID = $nextChildID;
        $account->lastChildID = $lastChildID;
        $account->parentID = $request->parentID;
        $account->suspended = $request->suspended;
        $account->save();

        if (!$account->save()) {
            return parent::getResponse(
                __('accountMessages.cannotInsert'),
                304
            );
        }

        return response()->json($account, 201);
    }

    public function update(Request $request)
    {
        $request->validate([
            'accountID' => 'required',
            'name' => 'required',
        ]);


        $account = Account::find($request->accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                404
            );
        }

        $nextChildID = null;
        $lastChildID = null;
        if ($request->main) {
            switch ($request->lastChildID) {
                case 9:
                    $nextChildID = $account->accountCode . '1';
                    $lastChildID = $account->accountCode . '9';
                    break;
                case 99:
                    $nextChildID = $account->accountCode . '01';
                    $lastChildID = $account->accountCode . '99';
                    break;
                case 999:
                    $nextChildID = $account->accountCode . '001';
                    $lastChildID = $account->accountCode . '999';
                    break;
                case 9999:
                    $nextChildID = $account->accountCode . '0001';
                    $lastChildID = $account->accountCode . '9999';
                    break;
            }
        }
        if (($account->parentID || $request->parentID != null) && !$account->lastChildID) {
            $account->name = $request->name;
            $account->main = $request->main;
            $account->nextChildID = $nextChildID;
            $account->lastChildID = $lastChildID;
        }
        if (!$account->lastChildID && !$account->parentID) {
            $account->name = $request->name;
            $account->type = strtoupper($request->type);
            $account->main = $request->main;
            $account->nextChildID = $nextChildID;
            $account->lastChildID = $lastChildID;
            $account->close = strtoupper($request->close);
            $account->parentID = $request->parentID;
            $account->suspended = $request->suspended;
        }
        if (($account->parentID || $request->parentID != null) && $account->lastChildID) {
            $account->name = $request->name;
        }
        $isDirty = $account->isDirty();

        $account->save();

        if (!$isDirty) {
            return parent::getResponse(
                __('accountMessages.noUpdates', ['name' => $account->name]),
                200
            );
        }

        if (!$account->save()) {
            return parent::getResponse(
                __('accountMessages.notUpdated', ['name' => $account->name]),
                304,
                $account
            );
        }

        return parent::getResponse(
            __('accountMessages.updated', ['name' => $account->name]),
            200,
            $account
        );
    }

    public function show($accountID) //add adjustment
    {
        $account = Account::find($accountID);

        if (!$account) {
            return parent::getResponse(
                __('accountMessages.notFound'),
                404
            );
        }

        return parent::getResponse(
            __('accountMessages.show'),
            200,
            $account
        );
    }

    public function list(Request $request)
    {
        if ($request->filter) {
            $accounts = Account::whereMain(true)->where(function ($query) use ($request) {
                $query->where('name', $request->filter)->orWhere('accountCode', $request->filter);
            })->limit(50)->get();
        } else {
            $accounts = Account::whereMain(true)->limit(50)->get();
        }
        $accounts = AccountListResource::collection($accounts);
        return parent::getResponse(
            __('accountMessages.index'),
            200,
            $accounts
        );
    }
}
