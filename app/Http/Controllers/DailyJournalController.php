<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DailyJournal;
use App\Http\Resources\JournalResources\ViewJournalResource;
use Illuminate\Validation\Rule;

class DailyJournalController extends Controller
{

    public function index()
    {
        $journals = DailyJournal::all();

        return parent::getPaginatedResopnse(
            __('dailyJournalMessages.index'),
            $journals
        );
    }

    public function JournalLastNumber()
    { //retreve journal number with no parents

        $journal = DailyJournal::max('number') + 1;
        if (!$journal) {
            $journal = 1;
        }
        return parent::getResponse(
            __('accountMessages.show'),
            200,
            $journal
        );
    }

    public function insert(Request $request)
    {
        $request->validate([
            'number' => 'required',
            'type' => [
                'required',
                Rule::in(['CREDIT', 'credit', 'DEBIT', 'debit', 'ADJUSTMENT', 'adjustment']),
            ],
            'date' => 'required',
            'state' => [
                'required',
                Rule::in(['DRAFT', 'POST', 'draft', 'post']),
            ],
        ]);

        $journal = new DailyJournal;

        $journal->number = $request->number;
        $journal->type = strtoupper($request->type);
        $journal->state = strtoupper($request->state);
        $journal->date = $request->date;
        $journal->statment = $request->statment;

        if (!$journal->save()) {
            return parent::getResponse(
                __('dailyJournalMessages.notInserted', ['name' => $request->name]),
                304,
                $journal
            );
        }

        return response()->json($journal, 201);
    }

    public function update(Request $request)
    {
        $request->validate([
            'journalID' => 'required',
            'number' => 'required',
            'type' => [
                'required',
                Rule::in(['CREDIT', 'credit', 'DEBIT', 'debit', 'ADJUSTMENT', 'adjustment']),
            ],
            'date' => 'required',
            'state' => [
                'required',
                Rule::in(['DRAFT', 'POST', 'draft', 'post']),
            ],
        ]);

        $journal = DailyJournal::find($request->journalID);

        if ($journal->state != 'DRAFT') {
            return parent::getResponse(
                __('dailyJournalMessages.cannotUpdate'),
                405
            );
        }

        if (!$journal) {
            return parent::getResponse(
                __('dailyJournalMessages.notFound'),
                404
            );
        }


        $journal->number = $request->number;
        $journal->type = strtoupper($request->type);
        $journal->state = strtoupper($request->state);
        $journal->date = $request->date;
        $journal->statment = $request->statment;
        $isDirty = $journal->isDirty();


        if (!$isDirty) {
            return parent::getResponse(
                __('dailyJournalMessages.noUpdates', ['name' => $request->name]),
                200
            );
        }

        if (!$journal->save()) {
            return parent::getResponse(
                __('dailyJournalMessages.notUpdated', ['name' => $request->name]),
                304,
                $journal
            );
        }

        return response()->json($journal, 201);
    }

    public function show($journalID)
    {
        $journal = DailyJournal::find($journalID);

        if (!$journal) {
            return parent::getResponse(
                __('journalMessages.notFound'),
                404
            );
        }

        $journal = ViewJournalResource::collection([$journal]);

        return parent::getResponse(
            __('journalMessages.show'),
            200,
            $journal
        );
    }

    public function delete($journalID)
    {
        $journal = DailyJournal::find($journalID);

        if (!$journal) {
            return parent::getResponse(
                __('dailyJournalMessages.notFound'),
                404
            );
        }
        if ($journal->state != 'DRAFT') {
            return parent::getResponse(
                __('dailyJournalMessages.cannotDelete'),
                405
            );
        }

        if (!$journal->delete()) {
            return parent::getResponse(
                __('dailyJournalMessages.notDeleted', ['name' => $journal->name]),
                304
            );
        }

        return parent::getResponse(
            __('dailyJournalMessages.deleted'),
            200
        );
    }
}
