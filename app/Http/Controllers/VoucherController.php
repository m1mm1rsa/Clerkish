<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Voucher;
use Illuminate\Validation\Rule;
use Exception;

use function PHPUnit\Framework\isEmpty;

class VoucherController extends Controller
{
    public function indexDebit()
    {
        $vouchers = Voucher::where('type', 'DEBIT');

        return parent::getPaginatedResopnse(
            __('voucherMessages.index'),
            $vouchers
        );
    }

    public function indexPayment()
    {
        $vouchers = Voucher::where('type', 'PAYMENT');

        return parent::getPaginatedResopnse(
            __('voucherMessages.index'),
            $vouchers
        );
    }

    public function lastPaymenttNumber()
    {
        $voucher = Voucher::where('type', 'PAYMENT')->max('number') + 1;

        if (!$voucher) {
            $voucher = 1;
        }
        return parent::getResponse(
            __('accountMessages.show'),
            200,
            $voucher
        );
    }

    public function lastDebitNumber()
    {
        $voucher = Voucher::where('type', 'DEBIT')->max('number') + 1;

        if (!$voucher) {
            $voucher = 1;
        }
        return parent::getResponse(
            __('accountMessages.show'),
            200,
            $voucher
        );
    }

    public function insert(Request $request, $type)
    {
        $request->validate([
            'paymentType' => [
                'required',
                Rule::in(['CARD', 'card', 'CASH', 'cash', 'TRANSFER', 'transfer', 'CHECK', 'check']),
            ],
            'date' => 'required',
            'amount' => 'required',
            'relatedPerson' => 'required',

        ]);

        $paymentData = $this->paymentWay($request);

        $paymentData = json_encode($paymentData);
        $number = 0;
        $number =  Voucher::where('type', $type)->max('number') + 1;

        $writtenAmount = $this->convertNumber($request->amount,'ريال','هللة');

        $voucher = new voucher;

        $voucher->number = $number;
        $voucher->type = strtoupper($type);
        $voucher->paymentType = strtoupper($request->paymentType);
        $voucher->date = $request->date;
        $voucher->writtenAmount = $writtenAmount;
        $voucher->paymentData = $paymentData;
        $voucher->amount = $request->amount;
        $voucher->relatedPerson = $request->relatedPerson;

        if (!$voucher->save()) {
            return parent::getResponse(
                __('voucherMessages.notInserted', ['name' => $request->name]),
                304,
                $voucher
            );
        }

        return response()->json($voucher, 201);
    }

    public function insertDebit(Request $request)
    {
        return $this->insert($request, 'DEBIT');
    }

    public function insertPayment(Request $request)
    {
        return $this->insert($request, 'PAYMENT');
    }

    public function update(Request $request)
    {
        $request->validate([
            'voucherID' => 'required',
            'number' => 'required|numeric',
            'paymentType' => [
                'required',
                Rule::in(['CARD', 'card', 'CASH', 'cash', 'TRASFER', 'transfer', 'CHECK', 'check']),
            ],
            'date' => 'required',
            'amount' => 'required',
            'relatedPerson' => 'required',
            'paymentData' => 'required',
        ]);

        $voucher = Voucher::find($request->voucherID);

        if (!$voucher) {
            return parent::getResponse(
                __('voucherMessages.notFound'),
                404
            );
        }
        $paymentData = $this->paymentWay($request);
        $paymentData = json_encode($paymentData);
        $writtenAmount = $this->convertNumber($request->amount,'ريال','هللة');

        $isDirty = false;
        $voucher->number = $request->number;
        $voucher->paymentType = strtoupper($request->paymentType);
        $voucher->date = $request->date;
        $voucher->amount = $request->amount;
        $voucher->writtenAmount = $writtenAmount;
        $voucher->paymentData = $request->paymentData;
        $voucher->relatedPerson = $request->relatedPerson;

        foreach ($request->name as $key => $localName) {
            if (
                !$voucher->name_j->offsetExists($key) ||
                $localName != $voucher->name_j[$key]
            ) {
                $voucher->name_j[$key] = $localName;
                $isDirty = true;
            }
        }

        if (!$isDirty) {
            return parent::getResponse(
                __('voucherMessages.noUpdates', ['name' => $voucher->name]),
                200
            );
        }

        if (!$voucher->save()) {
            return parent::getResponse(
                __('voucherMessages.notUpdated', ['name' => $request->name]),
                304,
                $voucher
            );
        }

        return response()->json($voucher, 201);
    }

    public function show($voucherID)
    {
        $voucher = Voucher::find($voucherID);

        if (!$voucher) {
            return parent::getResponse(
                __('voucherMessages.notFound'),
                404
            );
        }

        return parent::getResponse(
            __('voucherMessages.show'),
            200,
            $voucher
        );
    }

    public function delete($voucherID)
    {
        $voucher = Voucher::find($voucherID);

        if (!$voucher) {
            return parent::getResponse(
                __('voucherMessages.notFound'),
                404
            );
        }

        if (!$voucher->delete()) {
            return parent::getResponse(
                __('voucherMessages.notDeleted', ['name' => $voucher->name]),
                304
            );
        }

        return parent::getResponse(
            __('voucherMessages.deleted'),
            200
        );
    }

    public function addStatment(Request $request)
    {
        $request->validate([
            'voucherID' => 'required',
            'statment' => 'required'
        ]);

        $voucher = Voucher::find($request->voucherID);
        if (!$voucher) {
            return parent::getResopnse(
                __('voucherMessages.notFound'),
                404
            );
        }

        $voucher->statment = $request->statment;

        if (!$voucher->isDirty()) {
            return parent::getResponse(
                __('voucherMessages.noUpdates'),
                200
            );
        }


        if (!$voucher->save()) {
            return parent::getResponse(
                __('voucherMessages.cannotUpdate'),
                304
            );
        }
        return response()->json($voucher, 200);
    }

    public function paymentWay($request)
    {

        switch (strtoupper($request->paymentType)) {
            case 'CHECK':
                $request->validate([
                    'checkNumber' => 'required|numeric',
                    'checkDate' => 'required',
                    'bank' => 'required',
                ]);

                return $paymentData = [
                    'checkNumber' => $request->checkNumber,
                    'checkDate' => $request->checkDate,
                    'bank' => $request->bank,
                ];
                break;
            case 'TRANSFER':
                $request->validate([
                    'OperationNumber' => 'required|numeric',
                    'bank' => 'required',
                ]);

                return $paymentData = [
                    'OperationNumber' => $request->OperationNumber,
                    'bank' => $request->bank,
                ];
                break;
            case 'CARD':
                $request->validate([
                    'OperationNumber' => 'required|numeric',
                ]);

                return $paymentData = [
                    'OperationNumber' => $request->OperationNumber,
                ];
                break;
            default:
                return $paymentData = null;
        }
    }


    function convertNumber($number,$currency,$subcurrency)
    { 
        //split into integers and fraction.
        $number = number_format($number, 2, '.', '');
        $number = explode(".", $number);
        $fractionlen = strlen($number[1]);
        //start with only the integers.
        $digitslen = strlen($number[0]);
        $intnum = $number[0];

        //split the Integers from the highst value to the smallest. 
        $intnumarr = array_reverse(str_split($intnum, 1)); 

        //split the fraction from the highst value to the smallest.
        $fracnum = $number[1];
        $fractionlen = strlen($fracnum);
        $fracnumarr =  array_reverse(str_split($fracnum, 1));

        $Writinintegers = $this->fromNumToStr($digitslen, $intnumarr);
        $Writinfraction = $this->fromNumToStr($fractionlen, $fracnumarr);

        return $Writinintegers. ' ' .$currency . ' و' . $Writinfraction. ' ' .$subcurrency ;
    }
    function fromNumToStr($digitslen, $numarr)
    {
        $arb[18] = "";
        for ($i = 0; $i < $digitslen; $i++) {
            if (($i == 0 || $i == 3 || $i == 6 || $i == 9 || $i == 12 || $i == 15)) {//$i % 3 == 0
                $arb[$i] = "";
                if (array_key_exists($i + 2, $numarr)) {
                    $arb[$i] .= $numarr[$i]==0?"": "و";
                }
                switch ($numarr[$i]) {
                    case 0:
                        $arb[$i] .= " ";
                        break;
                    case 1:
                        $arb[$i] .= "واحد";
                        break;
                    case 2:
                        if ($numarr[$i + 1] = "1") $arb[$i] = " اثنا ";
                        else $arb[$i] = +" اثنان";
                        break;
                    case 3:
                        $arb[$i] .= " ثلاثة";
                        break;
                    case 4:
                        $arb[$i] .= " اربعة";
                        break;
                    case 5:
                        $arb[$i] .= " خمسة";
                        break;
                    case 6:
                        $arb[$i] .= " ستة";
                        break;
                    case 7:
                        $arb[$i] .= " سبعة";
                        break;
                    case 8:
                        $arb[$i] .= " ثمانية";
                        break;
                    case 9:
                        $arb[$i] .= " تسعة";
                        break;
                }
            }
            if ($i == 1 || $i == 4 || $i == 7 || $i == 10 || $i == 13 || $i == 16) {
                $arb[$i] = "";
                if ($numarr[$i - 1] != 0) { //check if the ones is 0
                    $arb[$i] .= $numarr[$i]==0?"": "و";
                }

                if ($numarr[$i] == 1) {
                    if ($numarr[$i - 1] == 0) {
                        $arb[$i] .= " عشرة";
                    } else {
                        $arb[$i] .= " عشر";
                    }
                }

                switch ($numarr[$i]) {
                    case 0:
                        $arb[$i] = " ";
                        break;
                    case 1:
                        if ($numarr[$i - 1] == "1") {
                            $arb[$i - 1] = " ";
                            $arb[$i] .= " أحد عشر";
                        }
                        break;
                    case 2:
                        $arb[$i] .= " عشرون";
                        break;
                    case 3:
                        $arb[$i] .= " ثلاثون";
                        break;
                    case 4:
                        $arb[$i] .= " اربعون";
                        break;
                    case 5:
                        $arb[$i] .= " خمسون";
                        break;
                    case 6:
                        $arb[$i] .= " ستون";
                        break;
                    case 7:
                        $arb[$i] .= " سبعون";
                        break;
                    case 8:
                        $arb[$i] .= " ثمانون";
                        break;
                    case 9:
                        $arb[$i] .= " تسعون";
                        break;
                }
            }
            if ($i == 2 || $i == 5 || $i == 8 || $i == 11 || $i == 14 || $i == 17) {
                $arb[$i] = "";
                if (array_key_exists($i + 1, $numarr)) {
                   $arb[$i] .= $numarr[$i]==0?"": "و";
                }
                switch ($numarr[$i]) {
                    case 0:
                        $arb[$i] .= " ";
                        break;
                    case 1:
                        $arb[$i] .= " مئة";
                        break;
                    case 2:
                        $arb[$i] .= " مئتان";
                        break;
                    case 3:
                        $arb[$i] .= " ثلاثمئة";
                        break;
                    case 4:
                        $arb[$i] .= " اربعمئة";
                        break;
                    case 5:
                        $arb[$i] .= " خمسمئة";
                        break;
                    case 6:
                        $arb[$i] .= " ستمئة";
                        break;
                    case 7:
                        $arb[$i] .= " سبعمئة";
                        break;
                    case 8:
                        $arb[$i] .= " ثمانمئة";
                        break;
                    case 9:
                        $arb[$i] .= " تسعمئة";
                        break;
                }
            }
            $numpos[16] = ""; //number position.
            if ($i == 0 || $i % 3 == 0) {
                switch ($i) {
                    case 0:
                        $numpos[$i] = "";
                        break;
                    case 3:
                        if (!array_key_exists($i + 1, $numarr) || ($numarr[$i + 1] == 0  || ($numarr[$i + 1] == 1 && $numarr[$i] == 0))) {
                            $numpos[$i] = " آلاف ";
                        } else {
                            $numpos[$i] = " ألف ";
                        }
                        break;
                    case 6:
                        if (!array_key_exists($i + 1, $numarr) || ($numarr[$i + 1] == 0  || ($numarr[$i + 1] == 1 && $numarr[$i] == 0))) {
                            $numpos[$i] = " ملايين ";
                        } else {
                            $numpos[$i] = " مليون ";
                        }
                        break;
                    case 9:
                        if (!array_key_exists($i + 1, $numarr) || ($numarr[$i + 1] == 0  || ($numarr[$i + 1] == 1 && $numarr[$i] == 0))) {
                            $numpos[$i] = " مليارات ";
                        } else {
                            $numpos[$i] = " مليار ";
                        }
                        break;
                    case 12:
                        if (!array_key_exists($i + 1, $numarr) || ($numarr[$i + 1] == 0  || ($numarr[$i + 1] == 1 && $numarr[$i] == 0))) {
                            $numpos[$i] = " بلايين ";
                        } else {
                            $numpos[$i] = " بليون ";
                        }
                        break;
                    case 15:
                        if (!array_key_exists($i + 1, $numarr) || ($numarr[$i + 1] == 0  || ($numarr[$i + 1] == 1 && $numarr[$i] == 0))) {
                            $numpos[$i] = " بليارات ";
                        } else {
                            $numpos[$i] = " بليار ";
                        }
                        break;
                }
            }
        }
        $strarb = "";
        $j = 0;
        $count = $digitslen;
        $temp = array();
        for ($i = 0; $i < $digitslen; $i += 3) {
            if ($count > 2) {
                $temp[$j] = $arb[$i + 2] . $arb[$i] . $arb[$i + 1] . $numpos[$i];
            } elseif ($count > 1) {
                $temp[$j] = $arb[$i] . $arb[$i + 1] . $numpos[$i];
            } else {
                $temp[$j] = $arb[$i] . $numpos[$i];
            }
            $count -= 3;
            $j++;
        }
        for ($i = count($temp) - 1; $i >= 0; $i--) {
            $strarb .= $temp[$i];
        }

        return $strarb;
    }
}
