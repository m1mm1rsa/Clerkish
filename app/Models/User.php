<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    public $timestamps = false;

    protected $hidden = [
        'password',
        'remember_token',
        'deleted_at',
       // 'active'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        //'active' => 'boolean'
    ];

    protected $appends = [
        'role_name'
    ];

    public function getRoleNameAttribute()
    {
        return __('userMessages.roles.' . $this->role);
    }
}
