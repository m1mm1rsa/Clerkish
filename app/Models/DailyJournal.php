<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\Sanctum;

class DailyJournal extends Model
{
    use HasFactory;

    protected $casts = [
        'number' => 'integer',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    public function accounts()
    {
        return $this->BelongsToMany(
            Account::class,
            'accounts_journals',
            'journalID',
            'accountID'
        )->withPivot(['credit','amount','statment']);
    }

    public function getAmountAttribute()
    {
        $amount = 0;
        $accounts = $this->accounts;

        foreach( $accounts as $account){
            $amount += $account->pivot->amount;
        }

        return $amount;
    }

}
