<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\Sanctum;

class Account extends Model
{
    use HasFactory;

    protected $casts = [
        'accountCode' => 'integer',
    ];

    public function parentAccount()
    {
        return $this->belongsTo(Account::class, 'parentID');
    }

    public function childAccounts()
    {
        return $this->hasMany(Account::class, 'parentID');
    }

    public function journals()
    {
        return $this->BelongsToMany(
            DailyJournal::class,
            'accounts_journals',
            'accountID',
            'journalID'
        )->withPivot(['credit','amount','statment']);
    }

}
