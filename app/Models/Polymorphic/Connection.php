<?php

namespace App\Models\Polymorphic;

use Illuminate\Database\Eloquent\Model;
use App\Models\Polymorphic\UserAction;

class Connection extends Model
{
    //Parent Methods
    public function save(array $options = [])
    {
        $actionType = $this->exists ? 'Update' : 'New';

        $response = parent::save($options);
        $userAction = new UserAction;
        $userAction->userID = auth()->user()->id;
        $userAction->actionType = $actionType;
        $this->userActions()->save($userAction);
        return $response;
    }

    public function delete()
    {
        $userAction = new UserAction;
        $userAction->userID = auth()->user()->id;
        $userAction->actionType = 'Delete';
        $this->userActions()->save($userAction);
        return parent::delete();
    }

    //End of Parent Methods

    public function connectable()
    {
        return $this->morphTo();
    }

    public function userActions()
    {
        return $this->morphMany(
            UserAction::class,
            'actionable',
            'relatedModel',
            'relatedID'
        );
    }
}
