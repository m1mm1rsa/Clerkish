<?php

namespace App\Models\Polymorphic;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserAction extends Model
{
    const UPDATED_AT = null;

    protected $hidden = [
        'updated_at'
    ];

    protected $appends = [
        'action_title'
    ];

    public function actionable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function getActionTitleAttribute()
    {
        return __('userMessages.actions.' . $this->actionType);
    }
}
