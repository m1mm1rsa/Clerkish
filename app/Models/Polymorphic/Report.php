<?php

namespace App\Models\Polymorphic;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserActionTrait;

class Report extends Model
{
    use UserActionTrait;

    protected $casts = [
        'startDate' => 'date',
        'endDate' => 'date',
    ];

    protected $appends = [
        'path_to'
    ];

    public function reportable()
    {
        return $this->morphTo();
    }

    public function getPathToAttribute()
    {
        return storage_path('app/'.$this->path);
    }
}
