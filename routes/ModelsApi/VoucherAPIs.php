<?php

use App\Http\Controllers\VoucherController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'vouchers'], function () {
    Route::group(['middleware' => 'auth:sanctum'], function () {

        Route::post('update', [VoucherController::class, 'update']);
        Route::post('addStatment', [VoucherController::class, 'addStatment']);
        Route::get('show/{voucherID}', [VoucherController::class, 'show']);
        Route::get('lastDebitNumber', [VoucherController::class, 'lastDebitNumber']);
        Route::get('lastPaymenttNumber', [VoucherController::class, 'lastPaymenttNumber']);
        Route::delete('delete/{voucherID}', [VoucherController::class, 'delete']);

        Route::group(['middleware' => 'admin'], function () {
            Route::get('indexDebit', [VoucherController::class, 'indexDebit']);//must split it to payment and debit.
            Route::get('indexPayment', [VoucherController::class, 'indexPayment']);
            Route::post('insertDebit', [VoucherController::class, 'insertDebit']);
            Route::post('insertPayment', [VoucherController::class, 'insertPayment']);
        });
    });
});
