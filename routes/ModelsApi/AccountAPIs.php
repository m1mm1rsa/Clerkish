<?php

use App\Http\Controllers\AccountController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'accounts'], function () {
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('update', [AccountController::class, 'update']);
        Route::get('show/{accountID}', [AccountController::class, 'show']);
        Route::get('lastAccountCode', [AccountController::class, 'lastAccountCode']);
        Route::post('insert', [AccountController::class, 'insert']);
        
        Route::group(['middleware' => 'admin'], function () {
            Route::get('list', [AccountController::class, 'list']);
            Route::get('index', [AccountController::class, 'index']);
        });
    });
});
