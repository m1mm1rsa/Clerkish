<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'users'], function () {
    Route::post('login', [UserController::class, 'login']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('update', [UserController::class, 'update']);
        Route::get('logout', [UserController::class, 'logout']);
        Route::post('resetPassword', [UserController::class, 'resetPassword']);
        Route::get('show/{userID}', [UserController::class, 'show']);
        Route::get('myProfile/{email}', [UserController::class, 'myProfile']);
        Route::delete('delete/{userID}', [UserController::class, 'delete']);

        Route::group(['middleware' => 'admin'], function () {
            Route::get('index', [UserController::class, 'index']);
            Route::post('insert', [UserController::class, 'insert']);
            Route::post('changerole', [UserController::class, 'changerole']);
        });
    });
});
