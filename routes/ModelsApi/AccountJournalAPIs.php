<?php

use App\Http\Controllers\AccountsJournalController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'accountJournals'], function () {
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('update', [AccountsJournalController::class, 'update']);
        Route::post('addStatment', [AccountsJournalController::class, 'addStatment']);
        Route::get('show/{accountID}/{journalID}', [AccountsJournalController::class, 'show']);
        Route::delete('delete/{accountID}/{journalID}', [AccountsJournalController::class, 'delete']);
        
        Route::group(['middleware' => 'admin'], function () {
            Route::get('index', [AccountsJournalController::class, 'index']);
            Route::post('insert', [AccountsJournalController::class, 'insert']);
        });
    });
});
